import gateway from 'temp-infrastructure-builder/src/resource/adopt/gateway.js';
import gatewayLambda from 'temp-infrastructure-builder/src/resource/adopt/gateway-lambda.js';

export default [
    gateway({ name: 'Example' }),
    gatewayLambda({
        name: 'Health',
        handler: 'health',
        codeUri: '../gateway',
        gateway: 'Example',
        gatewayPath: 'GET /health',
        gatewayInSameTemplate: true
    })
];
